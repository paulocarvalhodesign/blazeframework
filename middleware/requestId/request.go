package requestID

import (
	"net/http"
	"github.com/satori/go.uuid"
	"context"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		id := uuid.NewV4()
		ctx := context.WithValue(r.Context(), "requestid", id)
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	})
}
