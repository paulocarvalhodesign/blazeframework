package logger

import (
	"bitbucket.org/paulocarvalhodesign/blazeframework/utilities/statistics"
	"github.com/go-xweb/log"
	"net/http"
	"time"
)

func Middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h.ServeHTTP(w, r)
		statistics.StatisticsMap.AddStatistics(r.Method, r.RequestURI, "", time.Since(start))
		log.Printf(
			"%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			time.Since(start),
		)
	})
}
