package blaze

import (
	"github.com/go-zoo/bone"
	"net/http"
	"bitbucket.org/paulocarvalhodesign/blazeframework/router"
	"bitbucket.org/paulocarvalhodesign/blazeframework/utilities/statistics"
)

//Router Type
type Mux router.Mux

//Get Router
func GetRouter()(*bone.Mux){
	return  bone.New()
}

//Run Server instance
func Run(port string,  mux *bone.Mux){
	http.ListenAndServe(":"+port, mux)
}


//Get Statistics

func GetNodeStats() *statistics.URLMap{
	return statistics.StatisticsMap
}
