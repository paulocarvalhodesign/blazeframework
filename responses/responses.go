package responses

type APIResponse struct {
	RequestID string  `json:"requestId"`
	Meta     interface{} `json:"meta"`
	Messages interface{} `json:"messages"`
	Data     interface{} `json:"data"`

}
