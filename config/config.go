package config

import (
	"os"
	"github.com/astaxie/beego/config"
)


var filepath string

func init() {
	if(os.Getenv("BLAZE_ENV") == ""){
		os.Setenv("BLAZE_ENV","dev")
	}
	if (os.Getenv("BLAZE_ENV") == "testing") {
		SetPath(os.Getenv("BLAZE_TEST_PATH"))
		return
	} else {
		SetPath("conf/" + os.Getenv("BLAZE_ENV") + ".conf")
	}
}

func SetPath(path string) {
	filepath = path
}

func GetSection(section string) map[string]string {
	data := map[string]string{}
	iniconf, err := config.NewConfig("ini", filepath)
	if err != nil {
		return data
	}
	data, iniErr := iniconf.GetSection(section)
	if iniErr != nil {
		return data
	}
	return data
}

func GetSectionByConfigType(section string, configType string) map[string]string {
	data := map[string]string{}
	iniconf, err := config.NewConfig(configType, filepath)
	if err != nil {
		return data
	}
	data, iniErr := iniconf.GetSection(section)
	if iniErr != nil {
		return data
	}
	return data
}
