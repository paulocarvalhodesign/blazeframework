package config

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)


func TestGetSection(t *testing.T) {
	data := map[string]string{}
	Convey("Should be of type map[string]string{}", t, func() {
		data = GetSection("app")
		So(data, ShouldHaveSameTypeAs, map[string]string{})
	})

	Convey("Get App Config Section", t, func() {
		So(data["title"], ShouldEqual, "Blaze Framework testing")
	})

	Convey("Fail to Get Config Section", t, func() {
		data = GetSection("doesnotexits")
		So(data["title"], ShouldEqual, "")
	})

	Convey("Fail to Set Path", t, func() {
		SetPath("")
		data := GetSection("app")
		So(data["title"], ShouldEqual, "")
	})
}

