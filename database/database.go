package database

import (
	"github.com/gocraft/dbr"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"bitbucket.org/paulocarvalhodesign/blazeframework/config"
)

var Conn *dbr.Connection

type Database struct {
	Connection *dbr.Connection
}

func init(){
	LoadConnection()
}

func LoadConnection() {
	data := config.GetSection("database")
	Conn, _ = dbr.Open("mysql", fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8&parseTime=true",
		data["user"],
		data["password"],
		data["server"],
		data["port"],
		data["name"],
	), nil)
}

func GetMysqlDB() (*dbr.Session){
 return Conn.NewSession(nil)
}


